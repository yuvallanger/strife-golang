package main

/*

   Game of Strife is a simulation of a quorum sensing model.
   Copyright (C) 2011-2014  Yuval Langer

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

var DefaultParameters = Parameters{
	Generations:                10,
	RInitOdds:                  0,
	SInitOdds:                  0,
	GInitOdds:                  0,
	SignalThreshold:            3,
	CooperationEffectThreshold: 3,
	SRadius:                    1,
	PGRadius:                   1,
	BoardSize:                  10,
	D:                          1,
	MutOddsR:                   1e-4,
	MutOddsS:                   1e-4,
	MutOddsG:                   1e-4,
	BasalCost:                  100,
	CooperationCost:            30,
	SignalCost:                 3,
	ReceptorCost:               1,
	PublicGoodsEffect:          0.6}

var DefaultSettings = Settings{
	GenerationsPerSnapshotSample:  50,
	GenerationsPerFrequencySample: 50,
	DataFilename:                  "model-data.dat"}
