package main

/*

   Game of Strife is a simulation of a quorum sensing model.
   Copyright (C) 2011-2014  Yuval Langer

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import (
	"flag"
)

type Flags struct {
	Cpuprofileflag         string
	Settings_filename_flag string
}

func Init_flags() (flags Flags) {
	const (
		defaultCpuprofile = ""
		usageCpuprofile   = "Write cpu profile to file."

		defaultSettings = ""
		usageSettings   = "Settings filename."
	)
	flag.StringVar(&flags.Cpuprofileflag, "cpuprofile", defaultCpuprofile, "")
	flag.StringVar(&flags.Settings_filename_flag, "settings", defaultSettings, usageSettings)

	flag.Parse()
	return
}
