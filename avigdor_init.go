package main

/*

   Game of Strife is a simulation of a quorum sensing model.
   Copyright (C) 2011-2014  Yuval Langer

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import (
	"math/rand"
)

func (model *Model) initBoardStrain() {
	// init on the metal
	model.BoardStrain = make([][]int, model.Parameters.BoardSize)
	for row_i := range model.BoardStrain {
		model.BoardStrain[row_i] = make([]int, model.Parameters.BoardSize)
	}

	// init in the model
	for row_i := range model.BoardStrain {
		for col_i := range model.BoardStrain[row_i] {
			if rand.Float64() < model.Parameters.RInitOdds {
				model.BoardStrain[row_i][col_i] += 1
			}
			if rand.Float64() < model.Parameters.SInitOdds {
				model.BoardStrain[row_i][col_i] += 2
			}
			// In Avigdor's model, all public goods are wt.
			model.BoardStrain[row_i][col_i] += 4
		}
	}
}

func (model *Model) initBoardProd() {
	// init on the metal
	model.BoardProd = make([][]bool, model.Parameters.BoardSize)
	for i0 := range model.BoardProd {
		model.BoardProd[i0] = make([]bool, model.Parameters.BoardSize)
	}

	// init in the model
	centerCoord := Coordinate{}
	for centerCoord.r = range model.BoardProd {
		for centerCoord.c = range model.BoardProd[centerCoord.r] {
			centerStrain := model.CellStrain(centerCoord)
			centerReceptor := r4strain[centerStrain]
			if model.CellSignalNum(centerCoord, centerReceptor) >= model.Parameters.SignalThreshold {
				model.SetCellProd(centerCoord, true)
			}
		}
	}
}

func (model *Model) initBoardPGNum() {
	model.BoardPGNum = make([][]int, model.Parameters.BoardSize)
	for row_i := range model.BoardPGNum {
		model.BoardPGNum[row_i] = make([]int, model.Parameters.BoardSize)
	}

	center_coord := Coordinate{}
	for center_coord.r = range model.BoardPGNum {
		for center_coord.c = range model.BoardPGNum[center_coord.r] {
			rad_coord := Coordinate{}
			for rad_coord.r = center_coord.r - model.Parameters.PGRadius; rad_coord.r < center_coord.r+model.Parameters.PGRadius+1; rad_coord.r++ {
				for rad_coord.c = center_coord.c - model.Parameters.PGRadius; rad_coord.c < center_coord.c+model.Parameters.PGRadius+1; rad_coord.c++ {
					rad_coord_t := rad_coord.ToroidCoordinates(model.Parameters.BoardSize)
					if model.CellProd(rad_coord_t) {
						model.AddToCellPGNum(center_coord, 1)
					}
				}
			}
		}
	}
}

func (model *Model) initBoards() {
	model.initBoardStrain()
	model.InitBoardSignalNum()
	model.initBoardProd()
	model.initBoardPGNum()
}
