package main

/*

   Game of Strife is a simulation of a quorum sensing model.
   Copyright (C) 2011-2014  Yuval Langer

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io/ioutil"
)

func (model *Model) SaveSnapshotsAsImages() {
	for snapshot_i, snapshot := range model.DataSamples.Snapshots {
		img := board_strain_to_image(&snapshot.Data)
		imagefile, err := ioutil.TempFile(".", fmt.Sprintf("image-%v-%05d-png-", model.StartTime, snapshot_i))
		if err != nil {
			panic(err)
		}
		png.Encode(imagefile, img)
		imagefile.Close()
	}
}

func board_strain_to_image(boardStrain *BoardStrain) (img *image.RGBA) {
	img = image.NewRGBA(image.Rect(0,
		0,
		len(*boardStrain),
		len((*boardStrain)[0])))
	for row := range *boardStrain {
		for col, strain := range (*boardStrain)[row] {
			img.Set(row, col, color.RGBA{
				uint8(255 * r4strain[strain]),
				uint8(255 * s4strain[strain]),
				uint8(255 * g4strain[strain]),
				255})
		}
	}
	return img
}
